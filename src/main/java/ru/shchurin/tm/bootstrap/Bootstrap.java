package ru.shchurin.tm.bootstrap;

import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.exception.*;
import ru.shchurin.tm.repository.ProjectRepository;
import ru.shchurin.tm.repository.TaskRepository;
import ru.shchurin.tm.service.ProjectService;
import ru.shchurin.tm.service.ProjectTaskService;
import ru.shchurin.tm.service.TaskService;
import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.util.DateUtil;

import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class Bootstrap {
    private static final String PROJECT_CLEAR = "PROJECT_CLEAR";
    private static final String PROJECT_CREATE = "PROJECT_CREATE";
    private static final String PROJECT_UPDATE = "PROJECT_UPDATE";
    private static final String PROJECT_LIST = "PROJECT_LIST";
    private static final String PROJECT_REMOVE = "PROJECT_REMOVE";
    private static final String TASK_CLEAR = "TASK_CLEAR";
    private static final String TASK_CREATE = "TASK_CREATE";
    private static final String TASK_UPDATE = "TASK_UPDATE";
    private static final String TASK_LIST = "TASK_LIST";
    private static final String TASK_REMOVE = "TASK_REMOVE";
    private static final String HELP = "HELP";
    private static final String EXIT = "EXIT";
    private static final String TASKS_OF_PROJECT = "TASKS_OF_PROJECT";

    private ProjectRepository projectRepository = new ProjectRepository();
    private TaskRepository taskRepository = new TaskRepository();
    private ProjectService projectService = new ProjectService(projectRepository);
    private TaskService taskService = new TaskService(taskRepository);
    private ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    public void init() throws IOException {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        boolean exit = true;
        while (exit) {
            String command = ConsoleUtil.getStringFromConsole();
            switch (command) {
                case PROJECT_CLEAR:
                    clearAllProject();
                    break;
                case PROJECT_CREATE:
                    createProject();
                    break;
                case PROJECT_UPDATE:
                    updateProject();
                    break;
                case PROJECT_LIST:
                    showAllProject();
                    break;
                case PROJECT_REMOVE:
                    removeProjectByName();
                    break;
                case TASK_CLEAR:
                    clearAllTask();
                    break;
                case TASK_CREATE:
                    createTask();
                    break;
                case TASK_UPDATE:
                    updateTask();
                    break;
                case TASK_LIST:
                    showAllTask();
                    break;
                case TASK_REMOVE:
                    removeTaskByName();
                    break;
                case HELP:
                    showAllCommand();
                    break;
                case EXIT:
                    exit = false;
                    break;
                case TASKS_OF_PROJECT:
                    showTasksOfProject();
                    break;
                default:
                    showMessageWrong();
                    break;
            }
        }
    }

    private void clearAllProject() {
        projectService.removeAll();
        System.out.println("[ALL PROJECT REMOVED]");
    }

    private void clearAllTask() {
        taskService.removeAll();
        System.out.println("[ALL TASK REMOVED]");
    }

    private void showAllProject() {
        System.out.println("[PROJECT LIST]");
        Collection<Project> projects = projectService.findAll();
        int index = 1;
        for (Project project : projects) {
            System.out.println(index++ + ". " + project);
        }
    }

    private void showAllTask() {
        System.out.println("[TASK LIST]");
        Collection<Task> tasks = taskService.findAll();
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index++ + ". " + task);
        }
    }

    private void createProject() throws IOException {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        String name = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER START_DATE:");
        String start = ConsoleUtil.getStringFromConsole();
        Date startDate;
        try {
            startDate = DateUtil.parseDate(start);
        } catch (ParseException e) {
            System.out.println("YOU ENTERED WRONG START_DATE:");
            return;
        }
        System.out.println("ENTER END_DATE:");
        String end = ConsoleUtil.getStringFromConsole();
        Date endDate;
        try {
            endDate = DateUtil.parseDate(end);
        } catch (ParseException e) {
            System.out.println("YOU ENTERED WRONG END_DATE:");
            return;
        }
        try {
            projectService.persist(new Project(name, startDate, endDate));
            System.out.println("[PROJECT CREATED]");
        } catch (ConsoleNameException | ConsoleStartDateException | ConsoleEndDateExceprion | AlreadyExistsException e) {
            System.out.println(e.getMessage());
        }
    }

    private void createTask() throws IOException {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        String name = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER START_DATE:");
        String start = ConsoleUtil.getStringFromConsole();
        Date startDate;
        try {
            startDate = DateUtil.parseDate(start);
        } catch (ParseException e) {
            System.out.println("YOU ENTERED WRONG START_DATE:");
            return;
        }
        System.out.println("ENTER END_DATE:");
        String end = ConsoleUtil.getStringFromConsole();
        Date endDate;
        try {
            endDate = DateUtil.parseDate(end);
        } catch (ParseException e) {
            System.out.println("YOU ENTERED WRONG END_DATE:");
            return;
        }
        System.out.println("ENTER PROJECT_ID:");
        String projectId = ConsoleUtil.getStringFromConsole();
        try {
            taskService.persist(new Task(name, projectId, startDate, endDate));
            System.out.println("[TASK CREATED]");
        } catch (ConsoleNameException | ConsoleStartDateException | ConsoleEndDateExceprion | AlreadyExistsException e) {
            System.out.println(e.getMessage());
        }
    }

    private void updateProject() throws IOException {
        System.out.println("[PROJECT UPDATE]");
        System.out.println("ENTER NAME:");
        String name = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER ID:");
        String id = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER START_DATE:");
        String start = ConsoleUtil.getStringFromConsole();
        Date startDate;
        try {
            startDate = DateUtil.parseDate(start);
        } catch (ParseException e) {
            System.out.println("YOU ENTERED WRONG START_DATE:");
            return;
        }
        System.out.println("ENTER END_DATE:");
        String end = ConsoleUtil.getStringFromConsole();
        Date endDate;
        try {
            endDate = DateUtil.parseDate(end);
        } catch (ParseException e) {
            System.out.println("YOU ENTERED WRONG END_DATE:");
            return;
        }
        Project project = new Project(id, name, startDate, endDate);
        try {
            projectService.merge(project);
            System.out.println("[PROJECT UPDATED]");
        } catch (ConsoleNameException | ConsoleStartDateException | ConsoleEndDateExceprion e) {
            System.out.println(e.getMessage());
        }
    }

    private void updateTask() throws IOException {
        System.out.println("[TASK UPDATE]");
        System.out.println("ENTER NAME:");
        String name = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER ID:");
        String id = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER PROJECT_ID:");
        String projectId = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER START_DATE:");
        String start = ConsoleUtil.getStringFromConsole();
        Date startDate;
        try {
            startDate = DateUtil.parseDate(start);
        } catch (ParseException e) {
            System.out.println("YOU ENTERED WRONG START_DATE:");
            return;
        }
        System.out.println("ENTER END_DATE:");
        String end = ConsoleUtil.getStringFromConsole();
        Date endDate;
        try {
            endDate = DateUtil.parseDate(end);
        } catch (ParseException e) {
            System.out.println("YOU ENTERED WRONG END_DATE:");
            return;
        }
        Task task = new Task(id, name, projectId, startDate, endDate);
        try {
            taskService.merge(task);
            System.out.println("[PROJECT UPDATED]");
        } catch (ConsoleNameException | ConsoleStartDateException | ConsoleEndDateExceprion e) {
            System.out.println(e.getMessage());
        }
    }

    private void removeProjectByName() throws IOException {
        System.out.println("ENTER PROJECT NAME:");
        String name = ConsoleUtil.getStringFromConsole();
        try {
            projectTaskService.removeProjectAndTasksByName(name);
        } catch (ConsoleNameException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("[PROJECT AND HIS TASKS REMOVED]");
    }

    private void removeTaskByName() throws IOException {
        System.out.println("ENTER TASK NAME:");
        String name = ConsoleUtil.getStringFromConsole();
        try {
            taskService.removeByName(name);
        } catch (ConsoleNameException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("[TASK REMOVED]");
    }

    private void showAllCommand() {
        System.out.println("HELP: Show all commands.");
        System.out.println("PROJECT_CLEAR: Remove all projects.");
        System.out.println("PROJECT_CREATE: Create new projects.");
        System.out.println("PROJECT_LIST: Show all projects.");
        System.out.println("PROJECT_REMOVE: Remove selected project");
        System.out.println("TASK_CLEAR: Remove all task.");
        System.out.println("TASK_CREATE: Create new task.");
        System.out.println("TASK_LIST: Show all tasks.");
        System.out.println("TASK_REMOVE: Remove selected task.");
        System.out.println("TASKS_OF_PROJECT: Show all tasks of project.");
        System.out.println("PROJECT_ID: Show id of project.");
    }

    private void showTasksOfProject() throws IOException {
        System.out.println("[TASKS OF PROJECT]");
        System.out.println("ENTER PROJECT NAME");
        String name = ConsoleUtil.getStringFromConsole();
        List<Task> tasks;
        try {
            tasks = projectTaskService.getTasksOfProject(name);
            for (Task task : tasks) {
                System.out.println(task);
            }
        } catch (ConsoleNameException e) {
            System.out.println(e.getMessage());
        }
    }

    private void showMessageWrong() {
        System.out.println("YOU ENTERED WRONG COMMAND");
    }
}
