package ru.shchurin.tm;

import ru.shchurin.tm.bootstrap.Bootstrap;

import java.io.IOException;

public class Application {
    public static void main( String[] args ) throws IOException {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
